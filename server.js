const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const { Pool } = require('pg');

const app = express();
const port = 3000;

// Luodaan PostgreSQL-tietokantayhteys
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'loginform',
  password: '1',
  port: 5432,
});

// Palvele staattisia tiedostoja
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

// Näytä blogipostaukset
app.get('/api/data', async (req, res) => {
  try {
    const result = await pool.query('SELECT * FROM blog1');
    res.json(result.rows);
  } catch (error) {
    console.error('Virhe tietojen noutamisessa:', error);
    res.status(500).json({ error: 'Sisäinen palvelinvirhe' });
  }
});

// Lisää blogipostaus
app.post('/api/data', async (req, res) => {
  try {
    const { otsikko, postaus } = req.body;
    const result = await pool.query('INSERT INTO blog1 (otsikko, postaus) VALUES ($1, $2) RETURNING *', [otsikko, postaus]);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Virhe tietojen lisäämisessä:', error);
    res.status(500).json({ error: 'Sisäinen palvelinvirhe' });
  }
});
app.delete('/api/data/:postId', async (req, res) => {
  const postId = req.params.postId;

  try {
    const result = await pool.query('DELETE FROM blog1 WHERE id = $1 RETURNING *', [postId]);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error deleting post:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

//HTML-tiedostoja
const initialPath = path.join(__dirname, 'public');
app.get('/', (req, res) => {
    res.sendFile(path.join(initialPath, 'blogform.html'));
  });

app.get('/', (req, res) => {
  res.sendFile(path.join(initialPath, 'index.html'));
});

app.get('/login', (req, res) => {
  res.sendFile(path.join(initialPath, 'login.html'));
});

app.get('/register', (req, res) => {
  res.sendFile(path.join(initialPath, 'register.html'));
});

// Rekisteröi käyttäjä
app.post('/register-user', async (req, res) => {
  const { name, email, password } = req.body;

  if (!name.length || !email.length || !password.length) {
    res.json('Täytä kaikki kentät');
  } else {
    try {
      const result = await pool.query('INSERT INTO users (name, email, password) VALUES ($1, $2, $3) RETURNING name, email', [name, email, password]);
      res.json(result.rows[0]);
    } catch (error) {
      if (error.detail && error.detail.includes('already exists')) {
        res.json('Sähköposti on jo käytössä');
      } else {
        console.error('Virhe käyttäjän rekisteröinnissä:', error);
        res.status(500).json({ error: 'Sisäinen palvelinvirhe' });
      }
    }
  }
});

// Kirjaudu sisään
app.post('/login-user', async (req, res) => {
  const { email, password } = req.body;

  try {
    const result = await pool.query('SELECT name, email FROM users WHERE email = $1 AND password = $2', [email, password]);

    if (result.rows.length) {
      res.json(result.rows[0]);
    } else {
      res.json('Sähköposti tai salasana on väärä');
    }
  } catch (error) {
    console.error('Virhe kirjautumisessa:', error);
    res.status(500).json({ error: 'Sisäinen palvelinvirhe' });
  }
});

// Käynnistä palvelin
app.listen(port, () => {
  console.log(`Palvelin on käynnissä osoitteessa http://localhost:${port}`);
});
