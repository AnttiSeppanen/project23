document.addEventListener('DOMContentLoaded', () => {
  const dataForm = document.getElementById('dataForm');
  const dataList = document.getElementById('postaukset');
      
// julkaisujen hakeminen databasesta ja niiden näyttäminen
  async function fetchData() {
    try {
      const response = await fetch('http://localhost:3000/api/data');
      if (response.ok) {
        const data = await response.json();
        displayData(data);
      } else {
        console.error('Failed to fetch data:', response.statusText);
      }
    } catch (error) {
      console.error('Error:', error);
    }
  }
  async function deletePost(postId) {
    try {
      const response = await fetch(`/api/data/${postId}`, {
        method: 'DELETE',
      });

      if (response.ok) {
        console.log('Post deleted successfully');
      } else {
        console.error('Error deleting post:', response.statusText);
      }
    } catch (error) {
      console.error('Error deleting post:', error.message);
    }
  }

  // luo jokaiselle postaukselle oman elementin
  function displayData(data) {
    data.forEach(item => {
      const listItem = document.createElement('li');
      const deleteButton = document.createElement('button');
      deleteButton.textContent = 'Poista';
      deleteButton.onclick = () => deletePost(item.id);
      listItem.textContent = `ID: ${item.id}, Otsikko: ${item.otsikko}, Postaus: ${item.postaus}, julkaisija: ${sessionStorage.name}`;
      listItem.appendChild(deleteButton);
      dataList.appendChild(listItem);
    });
  }
  
  

  // lisää datan sivulle kun sivu päivitetään
  fetchData();


  dataForm.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(dataForm);
    const postaus = formData.get('postaus');
    const otsikko = formData.get('otsikko');

    console.log('Sending data');
    try {
      const response = await fetch('http://localhost:3000/api/data', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ otsikko, postaus }), 
      });

      if (response.ok) {
        const result = await response.json();
        console.log('Data sent successfully:', result);
      } else {
        console.error('Failed to send data:', response.statusText);
      }
    } catch (error) {
      console.error('Error:', error);
    }
  });
});
